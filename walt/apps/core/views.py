import datetime


from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Count
from django.contrib.auth.models import User
from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response, render, redirect
from django.template import loader, Context, RequestContext
from django.contrib.auth.decorators import login_required
from django.utils.translation import ugettext, ugettext_lazy
from lxml.etree import strip_tags
from plainbox.impl import public
from django.forms.models import modelformset_factory
from django.contrib.auth import login, authenticate, logout

from walt.apps.core.models import WallPost, WallCountry, Tweet


def archive(request):
    posts = WallPost.objects.all()
    poststwo = WallCountry.objects.all()
    #postsword = BlogPost.objects.filter().values_list('PostWord')
    #print(postsword)
    return render_to_response('archive.html', {'posts': posts,
                                               'poststwo': poststwo},
                              context_instance=RequestContext(request))


# Create your views here.

def AddUserPost(request):
    #if request.user.is_authenticated():
    addblogpost = modelformset_factory(WallPost)
    if request.method == 'POST':
        formset = addblogpost(request.POST, request.FILES, queryset=WallPost.objects.none())
        formset.timestamp = datetime.datetime.now
        if formset.is_valid():
            formset.timestamp = datetime.datetime.now
            formset.save()
    else:
        formset = addblogpost(queryset=WallPost.objects.none())

    return render_to_response('UserProfile.html', {'formset': formset},
                              context_instance=RequestContext(request))


class UserCreateForm(UserCreationForm):
    email = forms.EmailField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'Email'}))
    #first_name = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'First Name'}))
    #last_name = forms.CharField(required=True, widget=forms.widgets.TextInput(attrs={'placeholder': 'Last Name'}))
    username = forms.CharField(widget=forms.widgets.TextInput(attrs={'placeholder': ugettext_lazy("Username")}))
    password1 = forms.CharField(widget=forms.widgets.PasswordInput(attrs={'placeholder': ugettext_lazy("Password")}))
    password2 = forms.CharField(widget=forms.widgets.PasswordInput(attrs={'placeholder': ugettext_lazy("Password Confirmation")}))

    def is_valid(self):
        form = super(UserCreateForm, self).is_valid()
        for f, error in self.errors.items():
            if f != '__all_':
                self.fields[f].widget.attrs.update({'class': 'error', 'value': strip_tags(error)})
        return form

    class Meta:
        fields = ['username', 'email', 'first_name', 'last_name', 'password1',
                  'password2']
        model = User
        exclude = ['first_name', 'last_name']


class AuthenticateForm(AuthenticationForm):
    username = forms.CharField(widget=forms.widgets.TextInput(attrs={'placeholder': ugettext_lazy("Username")}))
    password = forms.CharField(widget=forms.widgets.PasswordInput(attrs={'placeholder': ugettext_lazy("Password")}))

    def is_valid(self):
        form = super(AuthenticateForm, self).is_valid()
        for f, error in self.errors.items():
            if f != '__all__':
                self.fields[f].widget.attrs.update({'class': 'error', 'value': strip_tags(error)})
        return form


class TweetForm(forms.ModelForm):
    content = forms.CharField(required=True, max_length=140, widget=forms.widgets.Input(attrs={'class': 'tweetText'}))
    CountryIdd = forms.widgets.Select(attrs={'class': 'CountryText'})

    def is_valid(self):
        form = super(TweetForm, self).is_valid()
        for f in self.errors.keys():
            if f != '__all__':
                self.fields[f].widget.attrs.update({'class': 'error tweetText'})
        return form

    class Meta:
        model = Tweet
        exclude = ('user',
                   #'PostImg'
                  )




def login_view(request):
    if request.method == 'POST':
        form = AuthenticateForm(data=request.POST)
        if form.is_valid():
            login(request, form.get_user())
            # Success
            return redirect('/')
        else:
            # Failure
            return index(request, auth_form=form)
    return redirect('/')


def logout_view(request):
    logout(request)
    return redirect('/')


def signup(request):
    user_form = UserCreateForm(data=request.POST)
    if request.method == 'POST':
        if user_form.is_valid():
            username = user_form.clean_username()
            password = user_form.clean_password2()
            user_form.save()
            user = authenticate(username=username, password=password)
            login(request, user)
            return redirect('/')
        else:
            return index(request, user_form=user_form)
    return redirect('/')


#@login_required
def submit(request):
    if request.user.is_authenticated():
         if request.method == "POST":
              tweet_form = TweetForm(data=request.POST, files=request.FILES)
              next_url = request.POST.get("next_url", "/")
              if tweet_form.is_valid():
                   tweet = tweet_form.save(commit=False)
                   tweet.user = request.user
                   tweet.save()
                   return redirect(next_url)
              else:
                   return public(request, tweet_form)
         return redirect('/')
    else:
        if request.method == "POST":
            tweet_form = TweetForm(data=request.POST, files=request.FILES)
            next_url = request.POST.get("next_url", "/")
            if tweet_form.is_valid():
                tweet = tweet_form.save(commit=False)
                tweet.user = request.user
                tweet.save()
                return redirect(next_url)
            else:
                return public(request, tweet_form)
        #return redirect('/')
        return render(request,
                      'submit.html',
                      {'tweet_form': tweet_form, })



@login_required
def public(request, tweet_form=None):
    tweet_form = tweet_form or TweetForm()
    tweets = Tweet.objects.reverse()[:10]
    return render(request,
                  'public.html',
                  {'tweet_form': tweet_form, 'next_url': '/tweets',
                   'tweets': tweets, 'username': request.user.username})


def get_latest(user):
    try:
        return user.tweet.order_by('-id')[0]
    except IndexError:
        return ""


@login_required
def users(request, username="", tweet_form=None):
    if username:
        # Show a profile
        try:
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            raise Http404
        tweets = Tweet.objects.filter(user=user.id)
        if username == request.user.username or request.user.profile.follows.filter(user__username=username):
            # Self Profile or buddies' profile
            return render(request, 'user.html', {'user': user, 'tweets': tweets, })
        return render(request, 'user.html', {'user': user, 'tweets': tweets, 'follow': True, })
    users = User.objects.all().annotate(tweets_count=Count('tweet'))
    tweets = map(get_latest, users)
    obj = zip(users, tweets)
    tweet_form = tweet_form or TweetForm()
    return render(request,
                  'profiles.html',
                  {'obj': obj, 'next_url': '/users/',
                   'tweet_form': tweet_form,
                   'username': request.user.username, })


@login_required
def follow(request):
    if request.method == "POST":
        follow_id = request.POST.get('follow', False)
        if follow_id:
            try:
                user = User.objects.get(id=follow_id)
                request.user.profile.follows.add(user.profile)
            except ObjectDoesNotExist:
                return redirect('/users/')
    return redirect('/users/')


def fully(request):
    posts = Tweet.objects.all()
    #postsword = BlogPost.objects.filter().values_list('PostWord')
    #print(postsword)
    t = loader.get_template("index.html")
    c = Context({'posts': posts})
    return HttpResponse(t.render(c))


def full(request, auth_form=None, user_form=None):
    # User is logged in
    if request.user.is_authenticated():
        tweet_form = TweetForm()
        posts = Tweet.objects.reverse().order_by('creation_date')[:200]
        user = request.user

        spisok = [0,50,100,150,180,200,250,300,350,400,450]
        return render(request,
                      'index.html',
                      {'tweet_form': tweet_form, 'user': user,
                       'posts': posts,
                       'spisok': spisok,
                       'next_url': '/', })
    else:
        # User is not logged in
        auth_form = auth_form or AuthenticateForm()
        user_form = user_form or UserCreateForm()
        tweet_form = TweetForm()
        posts = Tweet.objects.reverse().order_by('creation_date')[:200]
        user = request.user
        spisok = [0,50,100,150,180,200,250,300,350,400,450]
        return render(request,
                      'index_no.html',
                      {'auth_form': auth_form,'spisok': spisok, 'user_form': user_form, 'tweet_form': tweet_form, 'user': user,
                       'posts': posts, }, )


def index(request, auth_form=None, user_form=None):
    # User is logged in
    global lenus
    if request.user.is_authenticated():
        tweet_form = TweetForm()

        posts_self = Tweet.objects.filter(CountryIdd_id=ugettext("18"))

        tweets_buddies = Tweet.objects.filter(CountryIdd_id=ugettext("18"))
        tweets = posts_self | tweets_buddies
        user = request.user
        lenu = tweets_buddies.count()
        element = 10
        print([lenu])
        for element in range(lenu):
            lenu = element + 10
            element = lenu + 10
            print(lenu)

        spisok = [0,50,100,150,180,200,250,300,350,400,450]







        return render(request,
                      'buddies.html',
                      {'tweet_form': tweet_form, 'user': user,
                       'tweets': tweets,
                       'lenu': lenu,
                       'spisok': spisok,
                       'next_url': '/', })

    else:
        # User is not logged in
        auth_form = auth_form or AuthenticateForm()
        user_form = user_form or UserCreateForm()
        tweet_form = TweetForm()
        posts_self = Tweet.objects.filter(CountryIdd_id=ugettext("18"))

        tweets_buddies = Tweet.objects.filter(CountryIdd_id=ugettext("18"))
        posts = posts_self | tweets_buddies
        user = request.user
        spisok = [0,50,100,150,180,200,250,300,350,400,450]
        return render(request,
                      'buddies_full.html',
                      {'auth_form': auth_form, 'spisok': spisok, 'user_form': user_form, 'tweet_form': tweet_form, 'user': user,
                       'posts': posts, }, )


