from django.db import models
import datetime
from django.contrib import admin
from django.forms import ModelForm
from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError

class WallCountry(models.Model):
    PostText = models.CharField(max_length=140)
    #CountryId = models.ManyToManyField(WallPost)
    PostImg = models.ImageField(upload_to='img', blank=True,)
    def __unicode__(self):
        return u'%s'%(self.PostText)

class WallPost(models.Model):
    PostText = models.CharField(max_length=140)
    UserId = models.ForeignKey(User)
    CountryIdd = models.ForeignKey(WallCountry)
    PostImg = models.ImageField(upload_to='img', blank=True,)
    timestamp = models.DateTimeField(blank=True, default=datetime.datetime.now)

import hashlib




class Tweet(models.Model):
    content = models.CharField(max_length=140)
    user = models.ForeignKey(User,blank=True)
    CountryIdd = models.ForeignKey(WallCountry, default='1b')
    PostImg = models.ImageField(upload_to='img', blank=True,)
    creation_date = models.DateTimeField(auto_now=True, blank=True)


class Meta(Tweet):
    abstract = True
    ordering = ['creation_date']


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    follows = models.ManyToManyField('self', related_name='followed_by', symmetrical=False)
    UserImg = models.ImageField(upload_to='img', blank=True,)
    UserSoc = models.CharField(max_length=250)#ssylka na foto v social
    UserPro = models.CharField(max_length=250)#profile social
    UserGlaw = models.ImageField(upload_to='img', blank=True,)#profile social


    def gravatar_url(self):
        return "http://www.gravatar.com/avatar/%s?s=50" % hashlib.md5(self.user.email).hexdigest()
User.profile = property(lambda u: UserProfile.objects.get_or_create(user=u)[0])


    #form.instance.UserId = request.user
    #def save(self, *args, **kwargs):
        #super(BlogPost, self).save(*args, **kwargs)
        #img = PostImg.open(self.PostImg.path)
        #img.save(self.PostImg.path)

admin.site.register(WallPost)