from django.conf.urls import include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from walt.apps.core import views

from walt.settings import defaults



admin.autodiscover()




urlpatterns = i18n_patterns('',
    # Examples:
    # url(r'^$', 'mixer.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$', views.index), # root
    url(r'^login$', views.login_view), # login
    url(r'^place$', views.full),
    url(r'^logout$', views.logout_view), # logout
    url(r'^signup$', views.signup), # signup
    url(r'^submit$', views.submit), # submit new tweet
    url(r'^users/$', views.users),
    url(r'^users/(?P<username>\w{0,30})/$', views.users),
    url(r'^follow$', views.follow),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^hot/', views.archive),
    url(r'^hit/', views.AddUserPost),
    (r'^img/(?P<path>.*)$', 'django.views.static.serve',
{'document_root': defaults.MEDIA_ROOT, 'show_indexes': True }),
)
